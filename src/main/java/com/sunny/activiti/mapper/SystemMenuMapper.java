package com.sunny.activiti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sunny.activiti.entity.SystemMenu;

import java.util.List;

/**
 * <p>
 * 系统菜单表 Mapper 接口
 * </p>
 *
 * @author sunt
 * @since 2020-05-28
 */
public interface SystemMenuMapper extends BaseMapper<SystemMenu> {

}
